﻿using System;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper();
            wrapper.ListenQueue("Pong", "ping_queue", "pong_queue", 
                (message) =>
            {
                Console.WriteLine("Datetime now: {0}; Received message: {1}", DateTime.Now, message);
            }, wrapper.SendMessageToQueue);
            
            Console.WriteLine("Press [enter] to close.");
            Console.ReadLine();
            wrapper.Dispose();
            
        }
    }
}