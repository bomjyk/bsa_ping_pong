﻿using System;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper();
            wrapper.ListenQueue("Ping", "pong_queue", "ping_queue", 
                (message) =>
                {
                    Console.WriteLine("Datetime now: {0}; Received message: {1}", DateTime.Now, message);
                }, wrapper.SendMessageToQueue);
            wrapper.SendMessageToQueue("Ping", "pong_queue");
            Console.WriteLine("Press [enter] to close.");
            Console.ReadLine();
            wrapper.Dispose();
        }
    }
}