﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper
{
    public class Wrapper : IWrapper
    {
        private readonly IConfiguration _configuration;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        public Wrapper()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(String.Join('/',Directory.GetCurrentDirectory().Split('/').SkipLast(1)))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            _configuration = builder.Build();

            ConnectionFactory cf = new ConnectionFactory() {HostName = _configuration["ConnectionFactoryHostName"]};
            _connection = cf.CreateConnection();
            
            _channel = _connection.CreateModel();
            _channel.BasicQos(prefetchSize:0,prefetchCount:1,global:false);
            DeclareQueues();
        }

        private void DeclareQueues()
        {
            _channel.QueueDeclare(queue: _configuration.GetSection("queues")["ping"],
                durable: false,
                exclusive:false,
                autoDelete:true);
            _channel.QueueDeclare(queue: _configuration.GetSection("queues")["pong"],
                durable: false,
                exclusive:false,
                autoDelete:true);
        }
        public void ListenQueue(string respondMessage, string respondQueue, string queue, Action<string> handleRequest, Action<string,string> callback)
        {
            EventingBasicConsumer consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                string requestedMessage = Encoding.UTF8.GetString(ea.Body.ToArray());
                handleRequest(requestedMessage);
                callback(respondMessage, respondQueue);
                _channel.BasicAck(ea.DeliveryTag, false);
            };
            _channel.BasicConsume(queue: queue, autoAck: false, consumer: consumer); //TODO: Change autoAck
        }

        public void SendMessageToQueue(string message, string queue)
        {
            IBasicProperties prop = _channel.CreateBasicProperties();
            prop.Persistent = true;
            byte[] messageBody = Encoding.UTF8.GetBytes(message);
            Thread.Sleep(2500);
            _channel.BasicPublish(
                exchange:"",
                routingKey:queue,
                basicProperties:prop,
                body: messageBody);
        }

        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }
    }
}