using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    interface IWrapper : IDisposable
    {

        void ListenQueue(string respondMessage, string respondQueue, string queue, Action<string> handleRequest,
            Action<string, string> callback);

        void SendMessageToQueue(string message, string queue);
    }
}